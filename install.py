#!/usr/bin/env python

import os

print("")

# Pre-run clean up
if os.path.exists(".venv"):
    os.system("rm -r .venv")

# Create new virtuel python 3 environment
os.system("python3 -m venv .venv")

# Write .env file to be used with https://github.com/kennethreitz/autoenv
os.system("echo source `pwd`/.venv/bin/activate > .env")

# Install requirements
os.system(". .venv/bin/activate;pip install --upgrade pip")
os.system(". .venv/bin/activate;pip install -Ur requirements.txt")
os.system("chmod +x install.py")
os.system("chmod +x manage.py")

# Pre-commit hook that enforces pep8
if not os.path.exists(".git"):
    os.system("git init")

if os.path.exists("installscripts"):
    os.system("mv installscripts .installscripts")

if not os.path.exists(".git/hooks/pre-commit"):
    os.system("cp ./.installscripts/pre-commit .git/hooks")
    os.system("chmod +x .git/hooks/pre-commit")

if not os.path.exists(".gitignore"):
    os.system("cp ./.installscripts/gitignore .gitignore")

os.system("echo \"./../../../../\" > .venv/lib/python3.5/site-packages/extra.pth")
